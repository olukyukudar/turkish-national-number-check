def turkishNationalNumberCheck(tc):

    oddNumbers = 0
    evenNumbers = 0
    firstTenNumber = 0
    index = 1

    if len(tc) == 11:
        if int(tc[0]) != 0:
            for number in tc:
                firstTenNumber += int(number)
                if index > 9:
                    break
                if index % 2 == 0:
                    evenNumbers += int(number)
                else:
                    oddNumbers += int(number)
                index += 1
            if (oddNumbers * 7 - evenNumbers) % 10 == int(tc[9]) \
            and firstTenNumber % 10 == int(tc[10]):
                return True
    return False

tc = input ("Type your Turkish national number: ")


if turkishNationalNumberCheck(tc):
    print ("This national nuber is true")
else:
    print ("This national number is false")
